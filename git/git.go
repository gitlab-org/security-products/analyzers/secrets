package git

import (
	"fmt"
	"os/exec"
	"strings"
)

// IsRepository checks if the current directory is a git repository
// Returns true if it is, false if not, and an error if the check fails
func IsRepository() (bool, error) {
	cmd := exec.Command("git", "rev-parse", "--is-inside-work-tree")
	output, err := cmd.CombinedOutput()

	// Handle the specific "not a git repo" case (exit code 128)
	if exitErr, ok := err.(*exec.ExitError); ok && exitErr.ExitCode() == 128 {
		return false, nil
	}

	if err != nil {
		return false, fmt.Errorf("git repository check failed: %w", err)
	}

	return strings.TrimSpace(string(output)) == "true", nil
}
