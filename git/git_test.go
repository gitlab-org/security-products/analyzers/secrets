package git

import (
	"os"
	"os/exec"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestIsRepository(t *testing.T) {
	tests := []struct {
		name         string
		setup        func(t *testing.T, dir string)
		expectIsRepo bool
		expectErr    bool
	}{
		{
			name: "inside git repository",
			setup: func(t *testing.T, dir string) {
				cmd := exec.Command("git", "init")
				cmd.Dir = dir
				require.NoError(t, cmd.Run(), "failed to initialize git repository")
			},
			expectIsRepo: true,
			expectErr:    false,
		},
		{
			name: "outside git repository",
			setup: func(_ *testing.T, _ string) {
				// No setup - just a plain directory
			},
			expectIsRepo: false,
			expectErr:    false,
		},
		{
			name: "git not installed",
			setup: func(t *testing.T, _ string) {
				t.Setenv("PATH", "") // Simulate "git" command not found
			},
			expectIsRepo: false, // this value doesn't really matter if expectErr is true
			expectErr:    true,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			dir := t.TempDir()

			if tt.setup != nil {
				tt.setup(t, dir)
			}

			originalDir, err := os.Getwd()
			require.NoError(t, err, "failed to get current directory")
			defer func() {
				require.NoError(t, os.Chdir(originalDir), "failed to restore original directory")
			}()

			require.NoError(t, os.Chdir(dir), "failed to change to test directory")

			isRepo, err := IsRepository()

			if tt.expectErr {
				require.Error(t, err, "expected an error but got none")
			} else {
				require.NoError(t, err, "unexpected error from IsRepository")
				require.Equal(t, tt.expectIsRepo, isRepo, "unexpected repository state")
			}
		})
	}
}
