package gitleaks

import (
	"crypto/sha256"
	"fmt"
	"strings"

	log "github.com/sirupsen/logrus"

	report "gitlab.com/gitlab-org/security-products/analyzers/report/v5"
)

const defaultSha = "0000000"

// Secret represents a gitleaks leak
type Secret struct {
	StartLine   int    `json:"StartLine"`
	Context     string `json:"Context"`
	Secret      string `json:"Secret"`
	Rule        string `json:"RuleID"`
	Description string `json:"Description"`
	Commit      string `json:"Commit"`
	File        string `json:"File"`
	Message     string `json:"Message"`
	Author      string `json:"Author"`
	Date        string `json:"Date"`
}

// Identifiers returns a slice of report.Identifier for the Secret.
// It uses the Rule field as the identifier, falling back to the Description
// if Rule is empty.
func (s *Secret) Identifiers() []report.Identifier {
	ruleID := s.Rule
	if ruleID == "" {
		ruleID = s.Description
	}
	return []report.Identifier{
		{
			Type:  report.IdentifierType("gitleaks_rule_id"),
			Name:  fmt.Sprintf("Gitleaks rule ID %s", ruleID),
			Value: ruleID,
		},
	}
}

// Location returns a report.Location for the Secret.
// It constructs a report.Commit from the Secret's fields and includes it in the Location.
// If the Commit Sha is empty, it uses a default value.
func (s *Secret) Location() report.Location {
	commit := report.Commit{
		Author:  s.Author,
		Date:    s.Date,
		Message: s.Message,
		Sha:     s.Commit,
	}
	if commit.Sha == "" {
		commit.Sha = defaultSha
	}
	return report.Location{
		File:      s.File,
		LineStart: s.StartLine,
		LineEnd:   s.StartLine,
		Commit:    &commit,
	}
}

func (s *Secret) fingerprint() string {
	// Cleanup the source code extract from the report.
	sourceCode := strings.TrimSpace(s.Secret)
	// create code fingerprint using SHA256.
	h := sha256.New()
	if _, err := h.Write([]byte(sourceCode)); err != nil {
		log.Warnf("fingerprint err: %s", err)
	}
	return fmt.Sprintf("%x", h.Sum(nil))
}

// CompareKey generates a unique key for the Secret.
// It uses the Rule (falling back to Description if Rule is empty) and the File.
// If a fingerprint is available, it's included in the key.
// The key components are joined with colons.
func (s *Secret) CompareKey() string {
	rule := s.Rule
	if rule == "" {
		rule = s.Description
	}
	fingerprint := s.fingerprint()
	if fingerprint != "" {
		return strings.Join([]string{s.File, fingerprint, rule}, ":")
	}
	return strings.Join([]string{s.File, rule}, ":")
}

// DescriptionWithCommit returns a description of the secret, including a truncated commit SHA if available.
// If a commit SHA is present, it returns a formatted string with the description and the first 8 characters of the SHA.
// If no commit SHA is available, it returns the original description.
func (s *Secret) DescriptionWithCommit() string {
	if s.Commit != "" {
		truncatedSHA := s.Commit
		if len(s.Commit) >= 8 {
			truncatedSHA = s.Commit[:8]
		}
		return fmt.Sprintf("%s secret has been found in commit %s.", s.Description, truncatedSHA)
	}
	return s.Description
}

// WithRuleID returns a function that sets the Rule field of a Secret.
func WithRuleID(id string) func(*Secret) {
	return func(s *Secret) {
		s.Rule = id
	}
}

// WithFile returns a function that sets the File field of a Secret.
func WithFile(path string) func(*Secret) {
	return func(s *Secret) {
		s.File = path
	}
}

// WithDescription returns a function that sets the Description field of a Secret.
func WithDescription(description string) func(*Secret) {
	return func(s *Secret) {
		s.Description = description
	}
}

// WithCommit returns a function that sets the Commit field of a Secret.
func WithCommit(commitSHA string) func(*Secret) {
	return func(s *Secret) {
		s.Commit = commitSHA
	}
}

// WithStartLine returns a function that sets the StartLine field of a Secret.
func WithStartLine(lineNumber int) func(*Secret) {
	return func(s *Secret) {
		s.StartLine = lineNumber
	}
}
