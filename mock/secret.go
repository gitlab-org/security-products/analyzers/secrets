package mock

import "gitlab.com/gitlab-org/security-products/analyzers/secrets/v3/gitleaks"

// MakeSecret creates a new gitleaks.Secret with default values and applies the provided options.
// It takes variadic functional options to customize the Secret.
func MakeSecret(options ...func(*gitleaks.Secret)) gitleaks.Secret {
	secret := gitleaks.Secret{
		StartLine:   1,
		Context:     "context",
		Secret:      "secret",
		Rule:        "rule_id",
		Description: "description",
		Commit:      "commit",
		File:        "file",
		Message:     "message",
		Author:      "author",
		Date:        "date",
	}

	for _, opt := range options {
		opt(&secret)
	}

	return secret
}
