package main_test

import (
	"fmt"
	"os"
	"os/exec"
	"regexp"
	"strconv"
	"strings"
	"testing"

	"github.com/lucasjones/reggen"
	"github.com/pelletier/go-toml"
	"github.com/stretchr/testify/require"

	"gitlab.com/gitlab-org/security-products/analyzers/secrets/v3/gitleaks"
	"gitlab.com/gitlab-org/security-products/analyzers/secrets/v3/mock"
)

type GitleaksRulesConfig struct {
	Title     string
	AllowList AllowList
	Rules     []Rule
}

type AllowList struct {
	Description string
	Regexes     []string
	Commits     []string
	Files       []string
	Paths       []string
	Repos       []string
}

type Rule struct {
	ID          string
	Description string
	Regex       string
	File        string
	Path        string
	ReportGroup int
	Tags        []string
	Entropies   []struct {
		Min   string
		Max   string
		Group string
	}
	AllowList AllowList
}

var ExcludedRules = map[string]struct{}{
	"PKCS8 private key":                             {},
	"RSA private key":                               {},
	"SSH private key":                               {},
	"PGP private key":                               {},
	"systemd-machine-id":                            {},
	"Github Personal Access Token":                  {},
	"Github OAuth Access Token":                     {},
	"SSH (DSA) private key":                         {},
	"SSH (EC) private key":                          {},
	"Github App Token":                              {},
	"Github Refresh Token":                          {},
	"Shopify shared secret":                         {},
	"Shopify access token":                          {},
	"Shopify custom app access token":               {},
	"Shopify private app access token":              {},
	"Slack token":                                   {},
	"Stripe":                                        {},
	"PyPI upload token":                             {},
	"Google (GCP) Service-account":                  {},
	"GCP API key":                                   {},
	"GCP OAuth client secret":                       {},
	"Password in URL":                               {},
	"Heroku API Key":                                {},
	"Slack Webhook":                                 {},
	"Twilio API Key":                                {},
	"Age secret key":                                {},
	"Facebook token":                                {},
	"Twitter token":                                 {},
	"Adobe Client ID (Oauth Web)":                   {},
	"Adobe Client Secret":                           {},
	"Alibaba AccessKey ID":                          {},
	"Alibaba Secret Key":                            {},
	"Asana Client ID":                               {},
	"Asana Client Secret":                           {},
	"Atlassian API token":                           {},
	"Bitbucket client ID":                           {},
	"Bitbucket client secret":                       {},
	"Beamer API token":                              {},
	"Clojars API token":                             {},
	"Contentful delivery API token":                 {},
	"Contentful preview API token":                  {},
	"Databricks API token":                          {},
	"digitalocean-access-token":                     {},
	"digitalocean-pat":                              {},
	"digitalocean-refresh-token":                    {},
	"Discord API key":                               {},
	"Discord client ID":                             {},
	"Discord client secret":                         {},
	"Doppler API token":                             {},
	"Dropbox API secret/key":                        {},
	"Dropbox short lived API token":                 {},
	"Dropbox long lived API token":                  {},
	"Duffel API token":                              {},
	"Dynatrace API token":                           {},
	"EasyPost API token":                            {},
	"EasyPost test API token":                       {},
	"Fastly API token":                              {},
	"Finicity client secret":                        {},
	"Finicity API token":                            {},
	"Flutterwave public key":                        {},
	"Flutterwave secret key":                        {},
	"Flutterwave encrypted key":                     {},
	"Frame.io API token":                            {},
	"GoCardless API token":                          {},
	"Grafana API token":                             {},
	"Hashicorp Terraform user/org API token":        {},
	"Hashicorp Vault batch token":                   {},
	"Hubspot API token":                             {},
	"Intercom API token":                            {},
	"Intercom client secret/ID":                     {},
	"Linear API token":                              {},
	"Linear client secret/ID":                       {},
	"Lob API Key":                                   {},
	"Lob Publishable API Key":                       {},
	"Mailchimp API key":                             {},
	"Mailgun private API token":                     {},
	"Mailgun public validation key":                 {},
	"Mailgun webhook signing key":                   {},
	"Mapbox API token":                              {},
	"messagebird-api-token":                         {},
	"MessageBird API client ID":                     {},
	"New Relic user API Key":                        {},
	"New Relic user API ID":                         {},
	"New Relic ingest browser API token":            {},
	"npm access token":                              {},
	"Planetscale password":                          {},
	"Planetscale API token":                         {},
	"Postman API token":                             {},
	"Pulumi API token":                              {},
	"Rubygem API token":                             {},
	"Segment Public API token":                      {},
	"Sendgrid API token":                            {},
	"Sendinblue API token":                          {},
	"Sendinblue SMTP token":                         {},
	"Shippo API token":                              {},
	"Linkedin Client secret":                        {},
	"Linkedin Client ID":                            {},
	"Twitch API token":                              {},
	"Typeform API token":                            {},
	"Meta access token":                             {},
	"Oculus access token":                           {},
	"Instagram access token":                        {},
	"Yandex.Cloud IAM Cookie v1 - 1":                {},
	"Yandex.Cloud IAM Cookie v1 - 2":                {},
	"Yandex.Cloud IAM Cookie v1 - 3":                {},
	"Yandex.Cloud AWS API compatible Access Secret": {},
	"Tailscale key":                                 {},
}

var revocations = map[string]string{
	"aws":                          "gitleaks_rule_id_aws",
	"gitlab_personal_access_token": "gitleaks_rule_id_gitlab_personal_access_token",
	"pypi_upload_token":            "gitleaks_rule_id_pypi_upload_token",
	"gcp_api_key":                  "gitleaks_rule_id_gcp_api_key",
	"google_(gcp)_service-account": "gitleaks_rule_id_google_(gcp)_service-account",
	"gcp_oauth_client_secret":      "gitleaks_rule_id_gcp_oauth_client_secret",
}

func TestMain(m *testing.M) {
	setup()
	code := m.Run()
	shutdown()
	os.Exit(code)
}

func setup() {
	// download the latest rules to get a gitleaks.toml
	// Check if wget is installed
	if _, err := exec.LookPath("wget"); err != nil {
		panic("Error: wget is not installed but is required to run the unit tests. Please install wget and try again.")
	}

	// Check if unzip is installed
	if _, err := exec.LookPath("unzip"); err != nil {
		panic("Error: unzip is not installed but is required to run the unit tests. Please install unzip and try again.")
	}

	secretDetectionRulesProjectid := "60960406"
	secretDetectionRulesVersion := "0.3.0"
	zipFile := fmt.Sprintf("secret-detection-rules-v%s.zip", secretDetectionRulesVersion)
	url := fmt.Sprintf("https://gitlab.com/api/v4/projects/%s/packages/generic/secret-detection-rules/v%s/secret-detection-rules-v%s.zip", secretDetectionRulesProjectid, secretDetectionRulesVersion, secretDetectionRulesVersion)

	executeCommand("rm", "-rf", "dist")
	executeCommand("wget", url)
	executeCommand("unzip", zipFile)
	executeCommand("rm", zipFile)
	executeCommand("mv", "dist/all_rules.toml", "gitleaks.toml")
}

func shutdown() {
	// delete the downloaded gitleaks.toml
	executeCommand("rm", "gitleaks.toml")
	executeCommand("rm", "-rf", "dist")
}

func TestGitleaksTOMLRegexp(t *testing.T) {
	tcs := []struct {
		name        string
		pattern     string
		shouldMatch bool
	}{
		{
			name:        "Token within text",
			pattern:     "SOMETEXT%sSOMETEXT",
			shouldMatch: false,
		},
		{
			name:        "Token directly after text",
			pattern:     "SOMETEXT%s",
			shouldMatch: false,
		},
		{
			name:        "Token directly before text",
			pattern:     "%sSOMETEXT",
			shouldMatch: false,
		},
		{
			name:        "Token in quotes",
			pattern:     `password="%s"`,
			shouldMatch: true,
		},
		{
			name:        "Token within ()",
			pattern:     "this is the token(%s)",
			shouldMatch: true,
		},
		{
			name:        "Token by itself",
			pattern:     "%s",
			shouldMatch: true,
		},
	}

	gitleaks := parseGitleaksTOML(t)
	for _, rule := range gitleaks.Rules {
		if _, exists := ExcludedRules[rule.ID]; exists {
			continue
		}
		t.Run(rule.ID, func(t *testing.T) {
			r := regexp.MustCompile(rule.Regex)

			// generate a matching string
			token, err := reggen.Generate(rule.Regex, 100)
			require.NoError(t, err)

			// We're currently using the _same regex_ for random token generation
			// as we use for pattern matching. This presents a problem, since sometimes
			// our regex contains patterns that we want to match against, but not
			// necessarily generate tokens containing these characters.
			//
			// We use a capture group in the regex to allow us to split the entire regex
			// into two groups:
			//
			//   1. The capture group defines the portion of the regex to be used for random password generation.
			//   2. The entire regex is used for matching whether a password is present.
			//
			// See https://gitlab.com/gitlab-org/gitlab/-/issues/467617#note_1959073247 for more details.
			capturedMatch := r.FindStringSubmatch(token)
			// Not all of our regexes contain capture groups. We only manipulate the token if the
			// capture group is present.
			if capturedMatch != nil && len(capturedMatch) > 1 {
				token = capturedMatch[1]
			}

			for _, tc := range tcs {
				t.Run(tc.name, func(t *testing.T) {
					stringToMatch := fmt.Sprintf(tc.pattern, token)

					match := r.Match([]byte(stringToMatch))

					message := "Expected pattern %q to match %q, but it did not. Pattern ID: %q."
					if !tc.shouldMatch {
						message = "Expected pattern %q not to match %q, but it did. Pattern ID: %q"
					}

					require.Equal(t, match, tc.shouldMatch, message, rule.Regex, stringToMatch, rule.ID)
				})
			}
		})
	}
}

// test to demonstrate bugfix for https://gitlab.com/gitlab-org/gitlab/-/issues/458626
func TestBug458626(t *testing.T) {
	brokenRules := map[string]string{
		"gitlab_personal_access_token":     "glpat-nQlCf8gbL_WW1ZRx5Ie-",
		"gitlab_pipeline_trigger_token":    "glptt-mWxQWyoIE5uV634H9l5Qo6vv6GB9QDARKzK3qr7-",
		"gitlab_runner_registration_token": "GR1348941qJztp7DlEpwdwQsWoBc-",
		"gitlab_runner_auth_token":         "glrt-A1GNXjVZm_tao2A9jzD-",
		"gitlab_feed_token":                "feed_token=zTbr5RMN_ZlMLMlfKu1-",
		"gitlab_oauth_app_secret":          "gloas-qSicdf79OoHIaWzYlcEHsTaMBLGg5RMs8S87Jppa0PFaO03NG953Alf9hY6uWP--",
		"gitlab_feed_token_v2":             "glft-dq9FtEJnOzCBf7xhHeL-",
		"gitlab_kubernetes_agent_token":    "glagent-qkTwYDc_e7tUqteJhcQP3edq0vxT6n2HrPR1rtCNuHnDqxU0b-",
		"gitlab_incoming_email_token":      "glimt-vkOi--UPU6LTxe7FvsKur3zf-",
		"gitlab_deploy_token":              "gldt-7zgDpMtmgOQRb3A5loS-",
		"gitlab_scim_oauth_token":          "glsoat-RpX4olfATL4jNshlRSD-",
		"gitlab_ci_build_token":            "glcbt-5zs1h_MBtaIN9Qa1Wux-HIRD8-",
		// the rule corresponding to `Ionic API token` has a regex containing `(?i)[a-z0-9]`, which
		// "considers all case variations of letters, including Unicode case folding".
		// This is why the `ſ` character at the end of the following string is matched, because
		// `ſ` is "Latin small letter long s", and when case insensitivity is enabled via the `(?i)`
		// flag, `ſ` is considered equivalent to s.
		"Ionic API token": "ion_YuWqjt8XXzyKiTBV7jrEyODKWVtWup64DsHbD2PSpſ",
	}

	gitleaks := parseGitleaksTOML(t)

	for _, rule := range gitleaks.Rules {
		if brokenRules[rule.ID] == "" {
			continue
		}

		equalMatch := fmt.Sprintf(`password="%s"`, brokenRules[rule.ID])
		r := regexp.MustCompile(rule.Regex)
		match := r.Match([]byte(equalMatch))
		require.True(t, match, "Expected pattern %q to match %q, but it did not. Pattern ID: %q.", rule.Regex, equalMatch, rule.ID)
	}
}

func TestAWSAccessToken(t *testing.T) {
	gitleaks := parseGitleaksTOML(t)
	rule := findRule("AWS", gitleaks.Rules)

	r := regexp.MustCompile(rule.Regex)
	fakeAwsToken := "AKIA0000000000000000"

	tests := []struct {
		name        string
		testString  string
		shouldMatch bool
	}{
		{
			name:        "Token only",
			testString:  fakeAwsToken,
			shouldMatch: true,
		},
		{
			name:        "Token within text",
			testString:  fmt.Sprintf("sometext%ssometext", fakeAwsToken),
			shouldMatch: false,
		},
		{
			name:        "Token within ()",
			testString:  fmt.Sprintf("this is the token(%s)", fakeAwsToken),
			shouldMatch: true,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			require.Equal(t, r.Match([]byte(tt.testString)), tt.shouldMatch)
		})
	}
}

// TestGitleaksConfig validates the gitleaks.toml configuration file. Code borrowed from
// https://github.com/gitleaks/gitleaks/blob/e6934bf0a5b87024c40417bfc5e59cfa5539b63c/config/config.go#L95-L234.
func TestGitleaksConfig(t *testing.T) {
	tomlLoader := rulesConfig()
	for _, rule := range tomlLoader.Rules {
		// check and make sure the rule is valid
		if rule.Regex == "" && rule.Path == "" && rule.File == "" && len(rule.Entropies) == 0 {
			t.Fatalf("invalid gitleaks toml, %v is not an actionable rule ", rule)
		}
		re, err := regexp.Compile(rule.Regex)
		require.NoError(t, err)

		_, err = regexp.Compile(rule.File)
		require.NoError(t, err)

		_, err = regexp.Compile(rule.Path)
		require.NoError(t, err)

		// rule specific regexes
		for _, re := range rule.AllowList.Regexes {
			_, err := regexp.Compile(re)
			require.NoError(t, err)
		}

		// rule specific filenames
		for _, re := range rule.AllowList.Files {
			_, err := regexp.Compile(re)
			require.NoError(t, err)
		}

		// rule specific paths
		for _, re := range rule.AllowList.Paths {
			_, err := regexp.Compile(re)
			require.NoError(t, err)
		}
		for _, e := range rule.Entropies {
			min, err := strconv.ParseFloat(e.Min, 64)
			require.NoError(t, err)

			max, err := strconv.ParseFloat(e.Max, 64)
			require.NoError(t, err)

			if e.Group == "" {
				e.Group = "0"
			}
			group, err := strconv.ParseInt(e.Group, 10, 64)
			require.NoError(t, err)

			if int(group) >= len(re.SubexpNames()) {
				t.Fatalf("problem loading config: group cannot be higher than number of groups in regexp. rule %v", rule)
			} else if group < 0 {
				t.Fatalf("problem loading config: group cannot be lower than 0. rule %v", rule)
			} else if min > 8.0 || min < 0.0 || max > 8.0 || max < 0.0 {
				t.Fatalf("problem loading config: invalid entropy ranges, must be within 0.0-8.0 %v", rule)
			} else if min > max {
				t.Fatalf("problem loading config: entropy Min value cannot be higher than Max value %v", rule)
			}
		}
	}

	// global regex allowLists
	for _, allowListRegex := range tomlLoader.AllowList.Regexes {
		_, err := regexp.Compile(allowListRegex)
		require.NoError(t, err)
	}

	// global file name allowLists
	for _, allowListFileName := range tomlLoader.AllowList.Files {
		_, err := regexp.Compile(allowListFileName)
		require.NoError(t, err)
	}

	// global file path allowLists
	for _, allowListFilePath := range tomlLoader.AllowList.Paths {
		_, err := regexp.Compile(allowListFilePath)
		require.NoError(t, err)
	}

	// global repo allowLists
	for _, allowListRepo := range tomlLoader.AllowList.Repos {
		_, err := regexp.Compile(allowListRepo)
		require.NoError(t, err)
	}
}

func TestRevocationTypes(t *testing.T) {
	revocationRules := revocationRuleTypes()
	require.Equal(t, len(revocations), len(revocationRules), fmt.Sprintf("revocation_type rules should equal %d", len(revocations)))
	for _, rule := range revocationRules {
		secret := mock.MakeSecret(
			gitleaks.WithRuleID(rule.ID),
			gitleaks.WithDescription(rule.ID),
		)
		identifier := fmt.Sprintf("%s_%s", strings.ToLower(string(secret.Identifiers()[0].Type)), slugify(secret.Identifiers()[0].Value))
		require.Equal(t, revocations[slugify(rule.ID)], identifier, "should be equal")
	}
}

func findRule(ruleID string, rules []Rule) Rule {
	keyRules := make(map[string]Rule, len(rules))

	for _, r := range rules {
		keyRules[r.ID] = r
	}

	return keyRules[ruleID]
}

func parseGitleaksTOML(t *testing.T) *GitleaksRulesConfig {
	cfgReader, err := os.Open("./gitleaks.toml")
	require.NoError(t, err)

	gitleaks := &GitleaksRulesConfig{}

	toml.NewDecoder(cfgReader).Strict(true).Decode(gitleaks)
	return gitleaks
}

func executeCommand(command string, args ...string) {
	cmd := exec.Command(command, args...)
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	cmd.Run()
}

func revocationRuleTypes() []Rule {
	revocationRules := []Rule{}
	for _, rule := range rulesConfig().Rules {
		for _, tag := range rule.Tags {
			if tag == "revocation_type" {
				revocationRules = append(revocationRules, rule)
			}
		}
	}
	fmt.Println(revocationRules)
	return revocationRules
}

func rulesConfig() GitleaksRulesConfig {
	b, err := os.ReadFile("./gitleaks.toml")
	if err != nil {
		panic(err)
	}

	rulesConfig := GitleaksRulesConfig{}
	if err = toml.Unmarshal(b, &rulesConfig); err != nil {
		panic(err)
	}

	return rulesConfig
}

func slugify(str string) string {
	return strings.ReplaceAll(strings.ToLower(str), " ", "_")
}
