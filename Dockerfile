ARG POST_ANALYZER_SCRIPTS_VERSION=0.3.0
ARG TRACKING_CALCULATOR_VERSION=2.5.0

FROM registry.gitlab.com/gitlab-org/security-products/post-analyzers/scripts:${POST_ANALYZER_SCRIPTS_VERSION} AS scripts
FROM registry.gitlab.com/gitlab-org/security-products/post-analyzers/tracking-calculator:${TRACKING_CALCULATOR_VERSION} AS tracking

FROM golang:1.22-alpine AS build

ENV CGO_ENABLED=0 GOOS=linux
WORKDIR /go/src/app
COPY . .
# build the analyzer binary and automatically set the AnalyzerVersion
# variable to the most recent version from the CHANGELOG.md file
RUN CHANGELOG_VERSION=$(grep -m 1 '^## v.*$' "CHANGELOG.md" | sed 's/## v//') && \
    PATH_TO_MODULE=`go list -m` && \
    go build -ldflags="-X '$PATH_TO_MODULE/metadata.AnalyzerVersion=$CHANGELOG_VERSION'" -o analyzer

FROM alpine:latest

ARG SCANNER_VERSION
ENV SCANNER_VERSION ${SCANNER_VERSION:-8.23.3}
ENV SECRET_DETECTION_RULES_PROJECTID=60960406
ENV SECRET_DETECTION_RULES_VERSION=0.7.0

# Define report type for post-script to choose SD report
# as the input to tracking calculator
ENV REPORT_TYPE=secret-detection

RUN apk update && apk upgrade

RUN wget https://github.com/gitleaks/gitleaks/releases/download/v${SCANNER_VERSION}/gitleaks_${SCANNER_VERSION}_linux_x64.tar.gz  && \
    tar -xf gitleaks_${SCANNER_VERSION}_linux_x64.tar.gz -C /usr/local/bin/ && \
    chmod a+x /usr/local/bin/gitleaks && \
    rm -rf gitleaks_${SCANNER_VERSION}_linux_x64.tar.gz && \
    apk add --no-cache git && \
    addgroup -g 1000 gitlab && adduser -u 1000 -G gitlab -S -D gitlab

# Download rules from secret-detection-rules and replace gitleaks.toml
RUN wget https://gitlab.com/api/v4/projects/${SECRET_DETECTION_RULES_PROJECTID}/packages/generic/secret-detection-rules/v${SECRET_DETECTION_RULES_VERSION}/secret-detection-rules-v${SECRET_DETECTION_RULES_VERSION}.zip && \
    unzip secret-detection-rules-v${SECRET_DETECTION_RULES_VERSION}.zip && \
    rm secret-detection-rules-v${SECRET_DETECTION_RULES_VERSION}.zip && \
    mv dist/all_rules.toml /gitleaks.toml

RUN mkdir -p /etc/ssl/certs/ && \
    touch /etc/ssl/certs/ca-certificates.crt && \
    chmod g+w /etc/ssl/certs/ca-certificates.crt

COPY --from=build --chown=root:root /go/src/app/analyzer /analyzer-binary
COPY --from=scripts /start.sh /analyzer
COPY --from=tracking /analyzer-tracking /analyzer-tracking

ENTRYPOINT []
CMD ["/analyzer", "run"]
