package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"os/exec"
	"path/filepath"

	log "github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"

	ruleset "gitlab.com/gitlab-org/security-products/analyzers/ruleset/v3"
	"gitlab.com/gitlab-org/security-products/analyzers/secrets/v3/config"
	"gitlab.com/gitlab-org/security-products/analyzers/secrets/v3/git"
)

const (
	flagHistoricScan       = "full-scan"
	flagGitLogOps          = "git-log-options"
	flagExcludedPaths      = "excluded-paths"
	secretDetectionLogOpts = "SECRET_DETECTION_LOG_OPTIONS"
	envVarFullScan         = "SECRET_DETECTION_HISTORIC_SCAN"
	noChangesMsg           = "no changes detected"
	leaksExitCode          = "0"
	defaultCommitBeforeSHA = "0000000000000000000000000000000000000000"
	// Retrieve all possible commits (see https://git-scm.com/docs/shallow).
	maxGitDepth = 2147483647
)

func analyzeFlags() []cli.Flag {
	return []cli.Flag{
		&cli.StringFlag{
			Name:    flagGitLogOps,
			Usage:   "Gitleaks git log options",
			EnvVars: []string{secretDetectionLogOpts},
		},
		&cli.BoolFlag{
			Name:    flagHistoricScan,
			Usage:   "Runs an historic (all commits) scan on the repository",
			EnvVars: []string{envVarFullScan},
		},
	}
}

// loadRulesetConfig gets called to load the ruleset config in the `command` module.
func loadRulesetConfig(projectPath string) (*ruleset.Config, error) {
	// Load local custom config if available.
	rulesetPath := filepath.Join(projectPath, ruleset.PathSecretDetection)
	customRuleset, err := ruleset.Load(rulesetPath, "secrets", log.StandardLogger())
	if err != nil {
		return nil, err
	}

	return customRuleset, nil
}

// analyze runs the tools and produces a report containing issues for each detected secret leak.
func analyze(c *cli.Context, path string, rulesetConfig *ruleset.Config) (io.ReadCloser, error) {
	// Create a temporary file. Gitleaks can't output to stdout.
	tmpFile, err := os.CreateTemp("", "gitleaks-*.json")
	if err != nil {
		log.Errorf("Couldn't create temporary file: %v\n", err)
		return nil, err
	}

	// Determine the ruleset configuration path when passthroughs are used.
	pathGitleaksConfig, err := config.Path(rulesetConfig)
	if err != nil {
		return nil, err
	}

	// default gitleaks log level is `info`. If SECURE_DEBUG_LEVEL is set we
	// should pass that level down to gitleaks as well
	logLevel := "info"
	if os.Getenv("SECURE_LOG_LEVEL") != "" {
		logLevel = os.Getenv("SECURE_LOG_LEVEL")
	}

	defaultOptions := []string{"detect", "--report-path", tmpFile.Name(),
		"--report-format", "json", "--source", path,
		"--config", pathGitleaksConfig, "--exit-code", leaksExitCode,
		"--log-level", logLevel}

	// fetch commits if needed
	if err := gitFetch(c); err != nil {
		return nil, err
	}

	options, err := buildOptions(c, defaultOptions)
	if err != nil {
		return nil, err
	}

	cmd := exec.Command("gitleaks", options...)
	cmd.Env = os.Environ()
	if err := listenForOutput(cmd); err != nil {
		return nil, err
	}

	// start gitleaks command
	log.Debugf("Running gitleaks command: %s\n", cmd.String())
	if err := cmd.Start(); err != nil {
		return nil, err
	}

	// wait for gitleaks command to finish
	if err := cmd.Wait(); err != nil {
		return nil, err
	}

	return os.Open(tmpFile.Name())
}

func buildOptions(c *cli.Context, options []string) ([]string, error) {
	if c.IsSet(flagGitLogOps) {
		return append(options, "--log-opts", c.String(flagGitLogOps)), nil
	} else if c.Bool(flagHistoricScan) {
		return options, nil
	}
	// default options will run a --no-git scan
	return append(options, "--no-git"), nil
}

func listenForOutput(cmd *exec.Cmd) error {
	// Listen for stderr coming from gitleaks
	stderr, err := cmd.StderrPipe()
	if err != nil {
		return err
	}
	scannerStdErr := bufio.NewScanner(stderr)
	go func() {
		for scannerStdErr.Scan() {
			m := scannerStdErr.Text()
			log.Info(m)
		}
	}()

	// Listen for stdout coming from gitleaks
	stdout, err := cmd.StdoutPipe()
	if err != nil {
		return err
	}
	scannerStdOut := bufio.NewScanner(stdout)
	go func() {
		for scannerStdOut.Scan() {
			m := scannerStdOut.Text()
			log.Info(m)
		}
	}()
	return nil
}

func gitFetch(c *cli.Context) error {
	// Check if current directory is a git repository
	isRepo, err := git.IsRepository()
	if err != nil {
		log.Warnf("Failed to check git repository status: %v", err)
		return nil
	}
	if !isRepo {
		log.Warn("Not a git repository. Skipping git operations.")
		return nil
	}

	commitBeforeSHA := os.Getenv("CI_COMMIT_BEFORE_SHA")
	commitSHA := os.Getenv("CI_COMMIT_SHA")
	defaultBranch := os.Getenv("CI_DEFAULT_BRANCH")
	commitBranch := os.Getenv("CI_COMMIT_BRANCH")
	commitRefName := os.Getenv("CI_COMMIT_REF_NAME")
	mrDiffBaseSHA := os.Getenv("CI_MERGE_REQUEST_DIFF_BASE_SHA")

	runAndLog := func(cmd *exec.Cmd) error {
		output, err := cmd.CombinedOutput()
		if err != nil {
			log.Warnf("Error encountered when running git command: %s\noutput: %s\nerror:%v",
				cmd.String(),
				string(output),
				err)
			return err
		}
		return nil
	}

	fetchAllCommits := func(ref string) error {
		return runAndLog(exec.Command("git", "fetch", fmt.Sprintf("--depth=%d", maxGitDepth), "origin", ref))
	}
	if c.Bool(flagHistoricScan) {
		// need to get fetch --all for a historic scan
		if err := runAndLog(exec.Command("git", "fetch", "--all")); err != nil {
			log.Warn("Continuing with already checked out git environment")
		}
		return nil

	} else if defaultBranch == commitBranch {
		// do nothing, if the branch is the same as the default branch we don't
		// need to git fetch since we are scanning using `--no-git`
		return nil
	} else if c.IsSet(flagGitLogOps) {
		// if --log-opts is set already we cannot be sure of the depth of the scan
		// therefore we need to fetch the entire history of the branch
		if err := fetchAllCommits(commitRefName); err != nil {
			log.Warn("Continuing with already checked out git environment")
		}
		return nil
	} else if mrDiffBaseSHA != "" {
		// we're running on an MR branch and need to use the correct commit range
		commitBeforeSHA = mrDiffBaseSHA
	} else if commitBeforeSHA == defaultCommitBeforeSHA {
		// if this condition is met this is the first commit on a new branch
		// by default we don't need to fetch anything since `GIT_DEPTH` is set
		// to 50.
		// Set logOptions for gitleaks

		cmd := exec.Command("git", "rev-list", "HEAD", "--count")
		commitCount, err := cmd.CombinedOutput()
		if err != nil {
			return fmt.Errorf("get commit count: %w", err)
		}
		// We need to ensure that enough commits exist on this branch to perform
		// a commit-range scan. The ^ gets the parent, which is troublesome if there
		// is _no_ parent.
		// Note: count is inclusive of `commitSHA`, which is why we're not comparing
		// against "0".
		if string(commitCount) != "1" {
			c.Set(flagGitLogOps, fmt.Sprintf("%s^..%s", commitSHA, commitSHA))
		}

		return nil
	}

	// if this a Merge Request, fetch all possible commits for the branch
	err = fetchAllCommits(commitRefName)
	if err != nil {
		log.Warnf("Defaulting to scan only %s", commitSHA)
		c.Set(flagGitLogOps, fmt.Sprintf("%s^..%s", commitSHA, commitSHA))
		return nil
	}

	// make sure that the diff base exists
	// if not, the git history may have been rewritten
	if err := runAndLog(exec.Command("git", "cat-file", "-t", commitBeforeSHA)); err != nil {
		log.Warn("Failed to retrieve all the commits from the last Git push event due to a force push. Continuing with already checked out git environment")
		return nil
	}

	// Set logOptions for gitleaks.
	c.Set(flagGitLogOps, fmt.Sprintf("%s..%s", commitBeforeSHA, commitSHA))

	return nil
}
