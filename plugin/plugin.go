package plugin

import (
	"os"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v3/plugin"
)

// Match will always return true since the secrets analyzer is language agnostic
func Match(_ string, _ os.FileInfo) (bool, error) {
	return true, nil
}

func init() {
	plugin.Register("secrets", Match)
}
