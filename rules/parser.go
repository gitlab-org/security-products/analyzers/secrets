package rules

import (
	"fmt"

	"github.com/BurntSushi/toml"
	log "github.com/sirupsen/logrus"
)

// Parser represents a TOML parser for Rules.
type Parser struct {
	visitedPaths map[string]bool
}

// NewParser creates and returns a new Parser instance.
func NewParser() *Parser {
	return &Parser{
		visitedPaths: make(map[string]bool),
	}
}

// Parse reads a TOML file from the given filePath and decodes it into a Rules struct.
// It returns a pointer to the parsed Rules and any error encountered during the process.
//
// Parameters:
//   - filePath: A string representing the path to the TOML file to be parsed.
//
// Returns:
//   - *Rules: A pointer to the parsed Rules struct.
//   - error: An error if any occurred during file opening or TOML parsing, nil otherwise.
func (p *Parser) Parse(filePath string) (*Rules, error) {
	log.Debugf("Parsing rule file %s", filePath)
	if p.visitedPaths[filePath] {
		return nil, fmt.Errorf("circular dependency detected: %s", filePath)
	}

	p.visitedPaths[filePath] = true

	// Parse the TOML data
	var rules Rules
	if _, err := toml.DecodeFile(filePath, &rules); err != nil {
		return nil, fmt.Errorf("error parsing TOML: %w", err)
	}

	// Parse extend if it exists
	if rules.Extend.Path != "" {
		extendRules, err := p.Parse(rules.Extend.Path)
		if err != nil {
			return nil, err
		}
		rules.Rules = append(rules.Rules, extendRules.Rules...)
	}

	return &rules, nil
}
