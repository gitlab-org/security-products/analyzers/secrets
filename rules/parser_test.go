package rules_test

import (
	"path/filepath"
	"testing"

	"github.com/stretchr/testify/require"

	"gitlab.com/gitlab-org/security-products/analyzers/secrets/v3/mock"
	"gitlab.com/gitlab-org/security-products/analyzers/secrets/v3/rules"
)

func TestParse(t *testing.T) {
	testCases := []struct {
		name             string
		path             string
		expectedErrorStr string
		expectedRules    rules.Rules
	}{
		{
			name:             "file does not exist",
			path:             "does_not_exist.toml",
			expectedErrorStr: "error parsing TOML: open testdata/does_not_exist.toml: no such file or directory",
		},
		{
			name:             "file is invalid",
			path:             "invalid.toml",
			expectedErrorStr: "error parsing TOML: toml: line 1 (last key \"title\"): strings cannot contain newlines",
		},
		{
			name:          "file is empty",
			path:          "empty.toml",
			expectedRules: rules.Rules{},
		},
		{
			name: "file is valid",
			path: "default.toml",
			expectedRules: mock.MakeEmptyRules(
				[]rules.Rule{
					mock.MakeRule("testing_1"),
					mock.MakeRule("testing_2"),
				},
			),
		},
		{
			name: "file extends valid file",
			path: "extend.toml",
			expectedRules: mock.MakeEmptyRules(
				[]rules.Rule{
					mock.MakeRule("testing_extending_1"),
					mock.MakeRule("testing_1"),
					mock.MakeRule("testing_2"),
				},
			),
		},
		{
			name:             "file extends file that does not exist",
			path:             "extend-does-not-exist.toml",
			expectedErrorStr: "error parsing TOML: open testdata/does_not_exist.toml: no such file or directory",
		},
		{
			name:             "file extends file that is invalid",
			path:             "extend-invalid.toml",
			expectedErrorStr: "error parsing TOML: toml: line 1 (last key \"title\"): strings cannot contain newlines",
		},
		{
			name: "multiple extension levels",
			path: "extend-cascade-project.toml",
			expectedRules: mock.MakeEmptyRules(
				[]rules.Rule{
					mock.MakeRule("test_cascade_project"),
					mock.MakeRule("test_cascade_group"),
					mock.MakeRule("testing_1"),
					mock.MakeRule("testing_2"),
				},
			),
		},
		{
			name: "file extends file that is empty",
			path: "extend-empty.toml",
			expectedRules: mock.MakeEmptyRules(
				[]rules.Rule{
					mock.MakeRule("testing_extending_1"),
				},
			),
		},
		{
			name:             "detects circular dependency",
			path:             "circular-a.toml",
			expectedErrorStr: "circular dependency detected: testdata/circular-a.toml",
		},
	}

	for _, tt := range testCases {
		t.Run(tt.name, func(t *testing.T) {
			parser := rules.NewParser()
			parsedRules, err := parser.Parse(filepath.Join("testdata", tt.path))

			if tt.expectedErrorStr != "" {
				require.Equal(t, tt.expectedErrorStr, err.Error())
			} else {
				require.NoError(t, err)
				require.Equal(t, &tt.expectedRules.Rules, &parsedRules.Rules)
			}
		})
	}
}
