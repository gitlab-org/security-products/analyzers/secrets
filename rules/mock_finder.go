package rules

import (
	"github.com/stretchr/testify/mock"
)

// MockFinder is a mock implementation of the Finder interface
type MockFinder struct {
	mock.Mock
}

// NewMockFinder creates and returns a new NewMockFinder instance.
func NewMockFinder() *MockFinder {
	return &MockFinder{}
}

// Find is the mock implementation of the Find method
func (m *MockFinder) Find(ruleID string) (*Rule, error) {
	args := m.Called(ruleID)

	// If the first return value is nil, return nil for the *Rule
	if args.Get(0) == nil {
		return nil, args.Error(1)
	}

	// Otherwise, return the *Rule and error
	return args.Get(0).(*Rule), args.Error(1)
}
