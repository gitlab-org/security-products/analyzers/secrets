package rules

import "fmt"

// Finder defines an interface for finding rules by their ID.
type Finder interface {
	// Find searches for a rule by its ID and returns a pointer to the rule if found.
	// If the rule is not found, it returns an error.
	Find(ruleID string) (*Rule, error)
}

// Lookup provides efficient searching of Rules by their ID.
type Lookup struct {
	lookup map[string]Rule
}

// NewLookup creates a new Lookup instance with the provided rules.
func NewLookup(rules Rules) Lookup {
	lookUp := buildLookup(rules)
	return Lookup{
		lookup: lookUp,
	}
}

// Find searches for a rule by its ID and returns a pointer to the rule if found.
// If the rule is not found, it returns an error.
func (l *Lookup) Find(ruleID string) (*Rule, error) {
	if rule, ok := l.lookup[ruleID]; ok {
		return &rule, nil
	}

	return nil, fmt.Errorf("rule %s not found", ruleID)
}

// buildLookup creates a map of rule IDs to Rule objects from the provided Rules.
func buildLookup(rules Rules) map[string]Rule {
	lookup := make(map[string]Rule)
	for _, rule := range rules.Rules {
		if _, exists := lookup[rule.ID]; !exists {
			lookup[rule.ID] = rule
		}
	}
	return lookup
}
