package rules

import (
	"strings"
)

// Rules represents a collection of Rule objects with a title.
type Rules struct {
	Title string
	// Extend represents the path to a file containing additional rules to be included.
	Extend struct {
		Path string
	}

	Rules []Rule
}

// Rule represents a single rule with its properties.
type Rule struct {
	ID          string
	Title       string
	Description string
	Remediation string
}

// DescriptionAndRemediation combines the Description and Remediation of a Rule.
func (r Rule) DescriptionAndRemediation() string {
	combinedString := r.Description

	if r.Description != "" && r.Remediation != "" {
		combinedString += "\n\n"
	}

	combinedString += r.Remediation

	return strings.TrimSpace(combinedString)
}
