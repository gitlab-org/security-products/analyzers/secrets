package secrets

import (
	"encoding/json"
	"io"
	"os"
	"path/filepath"

	log "github.com/sirupsen/logrus"

	report "gitlab.com/gitlab-org/security-products/analyzers/report/v5"
	ruleset "gitlab.com/gitlab-org/security-products/analyzers/ruleset/v3"
	"gitlab.com/gitlab-org/security-products/analyzers/secrets/v3/gitleaks"
	"gitlab.com/gitlab-org/security-products/analyzers/secrets/v3/metadata"
	"gitlab.com/gitlab-org/security-products/analyzers/secrets/v3/rules"
)

const (
	maxSourceCodeLen = 1750
)

// SecureReportFormatter is a report formatter for Gitleaks secrets.
type SecureReportFormatter struct {
	rulesLookup rules.Finder
}

// NewSecureReportFormatter creates a SecureReportFormatter with the given rule lookup.
func NewSecureReportFormatter(rulesLookup rules.Finder) SecureReportFormatter {
	return SecureReportFormatter{
		rulesLookup: rulesLookup,
	}
}

// Build creates a report from Gitleaks secrets using the provided reader and ruleset configuration.
func (s SecureReportFormatter) Build(reader io.Reader, rulesetConfig *ruleset.Config) (*report.Report, error) {
	var secrets []gitleaks.Secret
	err := json.NewDecoder(reader).Decode(&secrets)
	if err != nil {
		log.Errorf("Couldn't parse the Gitleaks report: %v\n", err)
		return nil, err
	}

	vulns := make([]report.Vulnerability, 0, len(secrets))
	for _, secret := range secrets {
		rule, err := s.rulesLookup.Find(secret.Rule)
		if err != nil {
			log.Infof("Couldn't find the rule (%v) in the GitLab ruleset, assigning default values to the finding.\n", secret.Rule)
		}
		if secret.Commit == "" {
			// update file to use relative path if scan is from a `no-git`
			// gitleaks report
			cwd, err := os.Getwd()
			if err != nil {
				log.Errorf("Could not get working dir: %v\n", err)
			} else {
				relPath, err := filepath.Rel(cwd, secret.File)
				if err != nil {
					log.Errorf("Could not generate relative path to working dir: %v\n", err)
				} else {
					secret.File = relPath
				}
			}
		}

		title := secret.Description
		if rule != nil && rule.Title != "" {
			title = rule.Title
		}

		description := secret.DescriptionWithCommit()
		if rule != nil && rule.Remediation != "" {
			description = rule.DescriptionAndRemediation()
		}

		vulns = append(vulns, report.Vulnerability{
			Category:             metadata.Type,
			Scanner:              &metadata.IssueScanner,
			Name:                 title,
			Description:          description,
			CompareKey:           secret.CompareKey(),
			Severity:             report.SeverityLevelCritical,
			Confidence:           report.ConfidenceLevelUnknown,
			Location:             secret.Location(),
			Identifiers:          secret.Identifiers(),
			RawSourceCodeExtract: truncate(secret.Secret, maxSourceCodeLen),
		})
	}

	newReport := report.NewReport()
	newReport.Analyzer = metadata.AnalyzerID

	if rulesetConfig != nil {
		newReport.Config.Ruleset = rulesetConfig.Ruleset
		newReport.Config.Path = rulesetConfig.Path
	} else {
		newReport.Config.Path = ruleset.PathSecretDetection
	}

	newReport.Vulnerabilities = vulns

	return &newReport, nil
}

func truncate(str string, maxLen int) string {
	if len(str) > maxLen {
		return str[:maxLen-1]
	}
	return str
}
