require 'tmpdir'
require 'English'

require 'gitlab_secure/integration_test/docker_runner'
require 'gitlab_secure/integration_test/shared_examples/scan_shared_examples'
require 'gitlab_secure/integration_test/shared_examples/report_shared_examples'
require 'gitlab_secure/integration_test/spec_helper'

describe 'running image' do
  let(:fixtures_dir) { 'qa/fixtures' }
  let(:expectations_dir) { 'qa/expect' }

  def image_name
    ENV.fetch('TMP_IMAGE', 'secrets:latest')
  end

  def privileged
    ENV.fetch('PRIVILEGED', false)
  end

  def target_mount_dir
    '/app'
  end

  context 'with test project' do
    def parse_expected_report(expectation_name, report_name = "gl-secret-detection-report.json")
      path = File.join(expectations_dir, expectation_name, report_name)
      if ENV['REFRESH_EXPECTED'] == "true"
        # overwrite the expected JSON with the newly generated JSON
        FileUtils.cp(scan.report_path, File.expand_path(path))
      end
      JSON.parse(File.read(path))
    end

    let(:global_vars) do
      {
        'ANALYZER_INDENT_REPORT': 'true',
        # CI_PROJECT_DIR is needed for `post-analyzers/scripts` to
        # properly resolve file locations
        # https://gitlab.com/gitlab-org/security-products/post-analyzers/scripts/-/blob/25479eae03e423cd67f2493f23d0c4f9789cdd0e/start.sh#L2
        'CI_PROJECT_DIR': target_mount_dir,
        'SECURE_LOG_LEVEL': 'debug'
      }
    end

    let(:project) { 'any' }
    let(:variables) { {} }
    let(:command) { [] }
    let(:script) { nil }
    let(:offline) { false }
    let(:target_dir) { File.join(fixtures_dir, project) }

    let(:scan) do
      GitlabSecure::IntegrationTest::DockerRunner.run_with_cache(
        image_name, fixtures_dir, target_dir, @description,
        command: command,
        script: script,
        offline: offline,
        variables: global_vars.merge(variables),
        report_filename: 'gl-secret-detection-report.json',
        privileged: privileged,
        user: 'gitlab')
    end

    let(:report) { scan.report }

    context 'by default' do
      let(:project) { 'secrets' }
      let(:variables) do
        { 'GITLAB_FEATURES': 'vulnerability_finding_signatures' }
      end

      it_behaves_like 'successful scan'

      describe 'created report' do
        it_behaves_like 'non-empty report'
        it_behaves_like 'recorded report' do
          let(:recorded_report) {
            parse_expected_report(project)
          }
        end
        it_behaves_like 'valid report'
      end
    end

    context 'with custom rulesets' do
      context 'with file passthrough' do
        let(:project) { 'synthesize-file-passthrough'}
        let(:variables) do
          {
            'GITLAB_FEATURES':'sast_custom_rulesets'
          }
        end

        it_behaves_like 'successful scan'

        describe 'created report' do
          it_behaves_like 'non-empty report'
          it_behaves_like 'recorded report' do
            let(:recorded_report) {
              parse_expected_report('synthesize-file-passthrough')
            }
          end
          it_behaves_like 'valid report'
        end
      end

      context 'with raw passthrough' do
        let(:project) { 'synthesize-raw-passthrough'}
        let(:variables) do
          {
            'GITLAB_FEATURES':'sast_custom_rulesets'
          }
        end

        it_behaves_like 'successful scan'

        describe 'created report' do
          it_behaves_like 'non-empty report'
          it_behaves_like 'recorded report' do
            let(:recorded_report) {
              parse_expected_report('synthesize-raw-passthrough')
            }
          end
          it_behaves_like 'valid report'
        end
      end

      context 'with git passthrough' do
        let(:project) { 'synthesize-git-passthrough'}
        let(:variables) do
          {
            'GITLAB_FEATURES':'sast_custom_rulesets'
          }
        end

        it_behaves_like 'successful scan'

        describe 'created report' do
          it_behaves_like 'non-empty report'
          it_behaves_like 'recorded report' do
            let(:recorded_report) {
              parse_expected_report('synthesize-git-passthrough')
            }
          end
          it_behaves_like 'valid report'
        end
      end

      context 'with url passthrough' do
        let(:project) { 'synthesize-url-passthrough'}
        let(:variables) do
          {
            'GITLAB_FEATURES':'sast_custom_rulesets'
          }
        end

        it_behaves_like 'successful scan'

        describe 'created report' do
          it_behaves_like 'non-empty report'
          it_behaves_like 'recorded report' do
            let(:recorded_report) {
              parse_expected_report('synthesize-url-passthrough')
            }
          end
          it_behaves_like 'valid report'
        end
      end

      context 'with extend directive used' do
        context 'with file passthrough' do
          let(:project) { 'extend-with-file-passthrough'}
          let(:variables) do
            {
              'GITLAB_FEATURES':'sast_custom_rulesets'
            }
          end

          it_behaves_like 'successful scan'

          describe 'created report' do
            it_behaves_like 'non-empty report'
            it_behaves_like 'recorded report' do
              let(:recorded_report) {
                parse_expected_report('extend-with-file-passthrough')
              }
            end
            it_behaves_like 'valid report'
          end
        end

        context 'with git passthrough' do
          let(:project) { 'extend-with-git-passthrough'}
          let(:variables) do
            {
              'GITLAB_FEATURES':'sast_custom_rulesets'
            }
          end

          it_behaves_like 'successful scan'

          describe 'created report' do
            it_behaves_like 'non-empty report'
            it_behaves_like 'recorded report' do
              let(:recorded_report) {
                parse_expected_report('extend-with-git-passthrough')
              }
            end
            it_behaves_like 'valid report'
          end
        end

        context 'with url passthrough' do
          let(:project) { 'extend-with-url-passthrough'}
          let(:variables) do
            {
              'GITLAB_FEATURES':'sast_custom_rulesets'
            }
          end

          it_behaves_like 'successful scan'

          describe 'created report' do
            it_behaves_like 'non-empty report'
            it_behaves_like 'recorded report' do
              let(:recorded_report) {
                parse_expected_report('extend-with-url-passthrough')
              }
            end
            it_behaves_like 'valid report'
          end
        end
      end

      context 'when a specific rule version is set' do
        let(:variables) do
          {
            'SECRET_DETECTION_RULES_VERSION': 'v1.12.0'
          }
        end

        describe 'analyzer output' do
          it 'includes the specified rule version' do
            expect(scan.combined_output)
              .to match(%r{Using secret detection rules version from})
          end
        end
      end
    end
  end
end
